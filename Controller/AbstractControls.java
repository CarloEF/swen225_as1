package Controller;

import Model.Game;
import View.GUI;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Abstract class for the Control classes of all the Views. 
 *
 */
public abstract class AbstractControls {
    protected GUI theView;
    protected Game theModel;
    protected Controller theController;
    protected Border lineBorder = BorderFactory.createLineBorder(Color.BLACK, 2);
    protected Border padding = BorderFactory.createEmptyBorder(2,2,2,2);
    protected Border genericBorder = BorderFactory.createCompoundBorder(padding, lineBorder);

    public AbstractControls(Controller controller){
        theController = controller;
        theView = theController.getTheView();
        theModel = theController.getTheModel();
    }


}
