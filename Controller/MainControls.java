package Controller;

import Model.*;

import View.Main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This is the Main Board View which is shown during the Player's turn.   
 * From the View, the Player can roll the dice and move until they can Accuse,
 * Suggest or End their turn, taking them to the Suggest View, Accuse View or 
 * the Main View for the next Player respectively
 *
 */
public class MainControls extends AbstractControls{

    public MainControls(Controller controller){
        super(controller);
    }

    public EndTurnListener getEndTurnListener() {
        return new EndTurnListener();
    }

    public MovementArrowListener getArrowListener() {
        return new MovementArrowListener();
    }

    public AccuseClickListener getAccuseListener() {
        return new AccuseClickListener();
    }

    public SuggestClickListener getSuggestListener() {
        return new SuggestClickListener();
    }

    public DiceRollClick getDiceRollListener() {
        return new DiceRollClick();
    }

    public class DiceRollClick implements MouseListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            // Clicking the mouse!

            if(theModel.getGamePhase() == Game.GamePhase.AWAIT_DICE) {
                // Disable saving for the user
                theView.enableSaveGame(false);
                theController.setGameSaved(false);

                SwingWorker<Void, Integer> diceRoll = new SwingWorker<>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        for(int i = 0; i < 50; i++) {

                            theModel.setMovesRemaining(
                                    theModel.diceOne.roll() +
                                    theModel.diceTwo.roll());
                            theView.repaint();
                            Thread.sleep(25);
                        }

                        theModel.setGamePhase(Game.GamePhase.MOVE);
                        theView.getMain().setActiveButtons();
                        theView.repaint();
                        updateGameHeader(theView.getMain());
                        return null;
                    }
                };

                diceRoll.execute();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    /**
     * This listens to actions in the Main class, like arrow keys going up
     */
    public class MovementArrowListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            Game game = theModel;

            if(game.getGamePhase() == Game.GamePhase.MOVE && game.getMovesRemaining() > 0) {
                // Get Game board for checking.
                Board b = game.getGameBoard();
                Player p = game.getCurrentPlayer();
                Cell currentCell = p.getCurrentCell();
                HashMap<Board.Direction, Cell> neighbours = b.neighbourCells(currentCell);

                // Let's move the character based on the key we receive
                boolean success = false;
                switch(e.getKeyCode()) {
                    case(KeyEvent.VK_UP):
                        // Moving up in the world.
                        if(neighbours.containsKey(Board.Direction.UP))
                            success = b.move(p, neighbours.get(Board.Direction.UP));
                        break;
                    case(KeyEvent.VK_LEFT):
                        if(neighbours.containsKey(Board.Direction.LEFT))
                            success = b.move(p, neighbours.get(Board.Direction.LEFT));
                        break;
                    case(KeyEvent.VK_DOWN):
                        if(neighbours.containsKey(Board.Direction.DOWN))
                            success = b.move(p, neighbours.get(Board.Direction.DOWN));
                        break;
                    case(KeyEvent.VK_RIGHT):
                        if(neighbours.containsKey(Board.Direction.RIGHT))
                            success = b.move(p, neighbours.get(Board.Direction.RIGHT));
                        break;
                    default:
                        break;
                }

                if(success)
                    game.setMovesRemaining(game.getMovesRemaining() - 1);

                // Move successful, see if there's no other moves now so we can end their turn immediately.
                Map<Board.Direction, Cell> validMoves = b.getValidMoves(theModel.getCurrentPlayer(),
                        b.neighbourCells(theModel.getCurrentPlayer().getCurrentCell()));

                theView.repaint();

                if(validMoves.size() == 0 && theModel.getMovesRemaining() != 0) {
                    JOptionPane.showMessageDialog(null,
                            "No valid moves. Please end your turn.");
                    theModel.setMovesRemaining(0);
                }

                theView.getMain().setActiveButtons();
            }
            theView.getMain().setActiveButtons();
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

    public class AccuseClickListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // Disable saving for the user
            theView.enableSaveGame(false);
            theController.setGameSaved(false);
            theController.viewAccuse();
        }
    }

    public class SuggestClickListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            // Disable saving for the user
            theView.enableSaveGame(false);
            theController.setGameSaved(false);
            theController.viewSuggest();
        }
    }

    public class EndTurnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            theView.requestFocus();

            if(theModel.getMovesRemaining() == 0 && theModel.getGamePhase() == Game.GamePhase.MOVE) {
                // Enable saving for the user
                theView.enableSaveGame(true);
                // Turn can be ended.
                theModel.nextTurn();
                //If all other players have failed to Accuse
                if(theModel.getGamePhase() == Game.GamePhase.GAME_OVER)
                	theController.viewResult();
	            else {
	                theView.getMain().setActiveButtons();
	                updateGameHeader(theView.getMain());
	            }
                theView.repaint();

	            // If a player has been boxed in at the start of their turn then take them back to their starting square
	            if(theModel.getGameBoard().hasNoValidMoves(theModel.getCurrentPlayer())){
                    int n = JOptionPane.showConfirmDialog(theView, "You have been boxed in with no valid moves. " +
                            "Would you like to return to your starting square?", "No valid moves", JOptionPane.YES_NO_OPTION);
                    if (n == 0) {
                        theModel.getGameBoard().resetPlayer(theModel.getCurrentPlayer());
                    } else {
                        theModel.setMovesRemaining(0);
                        theModel.setGamePhase(Game.GamePhase.MOVE);
                    }
                    theView.getMain().setActiveButtons();
                    theView.repaint();
                }
            }
        }
    }

    public void updateGameHeader(Main main) {
        Player player = theModel.getCurrentPlayer();

        main.turnLabel.setText(player.getName() + "'s turn. " + theModel.getGamePhase().getMessage());
        main.turnLabel.setOpaque(true);
        main.turnLabel.setBackground(player.getCharacter().getColor());
        main.turnLabel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10,20,0,20),
                BorderFactory.createLineBorder(player.getCharacter().getColor())));
    }

}
