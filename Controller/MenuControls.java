package Controller;

import Model.Game;
import Model.Serializer;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * These are the controls of the top menu bar of the game screen. 
 * Where the user can save, start a new game or quit the program.
 * This is visible in all Views of the Game.
 */
public class MenuControls extends AbstractControls{

    public MenuControls(Controller controller){
        super(controller);
    }

    class NewGameListener implements ActionListener {
        public void actionPerformed(ActionEvent ae){
            theController.viewNewGame();
        }
    }

    public ActionListener getNewGameListener() { return new NewGameListener(); }

    class LoadGameListener implements ActionListener{
        public void actionPerformed(ActionEvent ae){
            theController.viewLoadGame();
        }
    }

    public ActionListener getLoadGameListener() { return new LoadGameListener(); }


    class SaveGameListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("."));

            int userSelection = fileChooser.showSaveDialog(theView.getSaveGame());

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                if (fileToSave == null) {
                    return;
                }
                if (!fileToSave.getName().toLowerCase().endsWith(".txt")) {
                    fileToSave = new File(fileToSave.getParentFile(), fileToSave.getName() + ".txt");
                }
                try {
                    FileWriter fW = new FileWriter(fileToSave);
                    Serializer s = new Serializer();
                    fW.write(s.saveGame(theController.getTheModel()));
                    fW.close();
                    theController.setGameSaved(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ActionListener getSaveGameListener() { return new SaveGameListener(); }

    class QuitToMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            if(theController.isGameNotSaved()){ // If the game is not saved then notify user
                int n = JOptionPane.showConfirmDialog(theView, "Game is not saved. Are you sure you want to quit to menu?", "Quit to menu", JOptionPane.YES_NO_OPTION);
                if(n == 0){
                    theController.setTheModel(Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6}));
                    theController.viewWelcome();
                }
            } else {
                theController.setTheModel(Game.initialiseNewGame(new String[]{"A", "B", "C", "D", "E", "F"}, new Integer[]{1, 2, 3, 4, 5, 6}));
                theController.viewWelcome();
            }

        }
    }

    public QuitToMenuListener getQuitToMenuListener() { return new QuitToMenuListener(); }


    class QuitGameListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            if (theController.isGameNotSaved()) { // If the game is not saved then notify user
                int n = JOptionPane.showConfirmDialog(theView, "Game is not saved. Are you sure you want to quit?", "Quit game", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    System.exit(0);
                }
            } else {
                int n = JOptionPane.showConfirmDialog(theView, "Are you sure you want to quit?", "Quit game", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    System.exit(0);
                }
            }
        }
    }

    public QuitGameListener getQuitGameListener() { return new QuitGameListener(); }

    class ExitWindowListener extends WindowAdapter {
        public void windowClosing(WindowEvent wE){
            if (theController.isGameNotSaved()) { // If the game is not saved then make sure user wants to exit
                int n = JOptionPane.showConfirmDialog(theView, "Game is not saved. Are you sure you want to quit?", "Quit game", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    theController.setGameSaved(true);
                    System.exit(0);
                }
            } else {
                int n = JOptionPane.showConfirmDialog(theView, "Are you sure you want to quit?", "Quit game", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    System.exit(0);
                }
            }
        }
    }

    public ExitWindowListener getExitWindowListener() { return new ExitWindowListener(); }
}