package Controller;

import Model.Game;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This View is where the User(s) can fill all the required information
 * needed to start a new game of Cluedo; Number of Players, their names 
 * and Characters. Then they are led to the Main View of the first Player's turn.
 *
 */
public class NewGameControls extends AbstractControls{

    public NewGameControls(Controller controller){
        super(controller);
    }

    class SubmitNewGameListener implements ActionListener{
        public void actionPerformed(ActionEvent actionEvent) {
            if(theView.getNewGame().detailsFilledOut()){
                theController.setTheModel(Game.initialiseNewGame(theView.getNewGame().getNames(), theView.getNewGame().getCharacters()));
                theController.viewMain();
            } else {
                JOptionPane.showMessageDialog(null, "Please ensure all details for all players are filled out correctly",
                        "Incomplete Setup: ", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    public ActionListener getSubmitNewGameListener() { return new SubmitNewGameListener(); }

    class BackToMenuListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            theController.viewWelcome();
        }
    }

    public ActionListener getBackToMenuListener(){
        return new BackToMenuListener();
    }
}
