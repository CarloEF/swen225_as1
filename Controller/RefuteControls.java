package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The Controller for 'Refuting' View. 
 * This goes back to the Main View when a Refutation Card is submitted. 
 */
public class RefuteControls extends AbstractControls{

    public RefuteControls(Controller controller){
        super(controller);
        theController = controller;
        theView = theController.getTheView();
        theModel = theController.getTheModel();
    }

    class RefuteSubmitListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            if(theView.getRefute().getSelectedCard() != null){
                theModel.setCardRefuted(theView.getRefute().getSelectedCard());
                theController.viewMain();
            }
        }
    }

    public ActionListener getRefuteSubmitListener(){
        return new RefuteSubmitListener();
    }

}
