package Controller;

import Model.Circumstance;
import Model.Game;
import View.GUI;
import View.Suggest;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This controls the View shown when the Player wants to make
 * a Suggestion. 
 * On submitting a complete Suggestion; it will move to the Refutation View if 
 * the Suggestion is refutable, else will send the Player back to the Main View.
 *
 */
public class SuggestControls extends AbstractControls {

    public SuggestControls(Controller controller){
        super(controller);
        theController = controller;
        theView = theController.getTheView();
        theModel = theController.getTheModel();
    }

    class SuggestSubmitListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            Circumstance suggest = theView.getSuggest().getSuggestion();
            if(suggest != null){
                theModel.setCurrentSuggestion(suggest);

                // Move accused character to the room
                theModel.getGameBoard().jumpCell(theModel.getPlayer(suggest.getCharacter()),
                                                    theModel.getCurrentPlayer().getCurrentCell());

                JOptionPane.showMessageDialog(null,
                        suggest.getCharacter().getDescription() + " has been moved to your room!");

                if(theModel.checkForRefutation()){
                    theController.viewRefute();
                } else {
                    theModel.getCurrentPlayer().setCanAccuse(true);
                    theController.viewMain();
                }
            }
        }
    }

    public ActionListener getSuggestSubmitListener(){
        return new SuggestSubmitListener();
    }
}
