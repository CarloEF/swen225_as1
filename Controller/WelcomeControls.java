package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controls the first View seen by the User(s) on running the Program.
 * In the Welcome View, the user caneither go to the Load Game View or
 * set up a new game in the New Game View. 
 *
 */
public class WelcomeControls extends AbstractControls{

    public WelcomeControls(Controller controller){
        super(controller);
    }

    class NewGameListener implements ActionListener {
        public void actionPerformed(ActionEvent ae){
            theController.viewNewGame();
        }
    }

    public ActionListener getNewGameListener() { return new NewGameListener(); }

    class LoadGameListener implements ActionListener{
        public void actionPerformed(ActionEvent ae){
            theController.viewLoadGame();
        }
    }

    public ActionListener getLoadGameListener() { return new LoadGameListener(); }
}
