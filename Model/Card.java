package Model;/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5092.1e2e91fc6 modeling language!*/


import java.awt.*;
import java.util.ArrayList;

public enum Card {
    CANDLESTICK("Candlestick", "U", Type.WEAPON),
    DAGGER("Dagger", "V", Type.WEAPON),
    LEAD_PIPE("Lead Pipe", "W", Type.WEAPON),
    REVOLVER("Revolver", "X", Type.WEAPON),
    ROPE("Rope", "Y", Type.WEAPON),
    SPANNER("Spanner", "Z", Type.WEAPON),

    BALLROOM("Ballroom", "b", Type.ROOM, Color.decode("#c3d8e9")),
    BILLARDROOM("Billiard Room", "B", Type.ROOM, Color.decode("#c3d8e9")),
    CONSERVATORY("Conservatory", "c", Type.ROOM, Color.decode("#c3d8e9")),
    DINING("Dining", "d", Type.ROOM, Color.decode("#c3d8e9")),
    HALL("Hall", "h", Type.ROOM, Color.decode("#c3d8e9")),
    KITCHEN("Kitchen", "k", Type.ROOM, Color.decode("#c3d8e9")),
    LIBRARY("Library", "l", Type.ROOM, Color.decode("#c3d8e9")),
    LOUNGE("Lounge", "L", Type.ROOM, Color.decode("#c3d8e9")),
    STUDY("Study", "s", Type.ROOM, Color.decode("#c3d8e9")),

    CORRIDOR("Corridor", "_", Type.ROOM, Color.decode("#f8ef6f")),
    INACCESSIBLE("Inaccessible", "*", Type.ROOM, Color.BLACK),

    MISS_SCARLETT("Miss Scarlett","1", Type.CHARACTER, Color.decode("#e82003")),
    COLONEL_MUSTARD("Colonel Mustard", "2", Type.CHARACTER, Color.decode("#e5b84e")),
    MRS_WHITE("Mrs. White", "3", Type.CHARACTER, Color.WHITE),
    MR_GREEN("Mr. Green", "4", Type.CHARACTER, Color.GREEN),
    MRS_PEAKCOCK("Mrs. Peacock", "5", Type.CHARACTER, Color.decode("#5cf8c0")),
    PROFESSOR_PLUM("Professor Plum", "6", Type.CHARACTER, Color.decode("#ebd1fe"));

    public enum Type {
        WEAPON,
        ROOM,
        CHARACTER
    }

    /**
     * Single character representation associated with the card
     */
    String c;

    String name;

    /**
     * Type of card it is
     */
    Type type;

    public Color getColor() {
        return color;
    }

    Color color;

    Card(String name, String c, Type type) {
        this.name = name;
        this.c = c;
        this.type = type;
        this.color = Color.BLACK;
    }

    Card(String name, String c, Type type, Color color) {
        this.name = name;
        this.c = c;
        this.type = type;
        this.color = color;
    }

    public String toString() {
        return this.c;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.name;
    }

    public Card.Type getType() {
        return this.type;
    }

    /**
     * Drawing the card on the screen.
     *
     * @param g graphics pane to draw on
     * @param x x co-ordinate of top left point on drawing
     * @param y y co-ordinate of top left point on drawing
     * @param width width of drawing
     * @param height height of drawing
     */
    public void draw(Graphics g, int x, int y, int width, int height){
        g.setColor(Color.BLACK);
        g.drawRect(x,y,width,height);

        int stringWidth = g.getFontMetrics().stringWidth(this.getDescription());
        g.drawString(this.getDescription(), x + width / 2 - stringWidth / 2, y + height / 2);

    }

    /**
     * Drawing a faded card on the screen (Card that cannot be used to refute)
     *
     * @param g graphics pane to draw on
     * @param x x co-ordinate of top left point on drawing
     * @param y y co-ordinate of top left point on drawing
     * @param width width of drawing
     * @param height height of drawing
     */
    public void drawFaded(Graphics g, int x, int y, int width, int height){
        g.setColor(Color.GRAY);
        g.fillRect(x,y,width,height);
        g.setColor(Color.BLACK);
        int stringWidth = g.getFontMetrics().stringWidth(this.getDescription());
        g.drawString(this.getDescription(), x + width / 2 - stringWidth / 2, y + height / 2);

    }

    /**
     * Drawing a card on the screen highlighted (Currently selected card)
     *
     * @param g graphics pane to draw on
     * @param x x co-ordinate of top left point on drawing
     * @param y y co-ordinate of top left point on drawing
     * @param width width of drawing
     * @param height height of drawing
     */
    public void highlight(Graphics g, int x, int y, int width, int height){
        draw(g,x,y,width,height);
        g.setColor(Color.GREEN);
        g.drawRect(x,y,width,height);
    }

    /**
     * Gets card of given tyep
     *
     * @param t Type Enum
     * @return List of cards
     */
    public static ArrayList<Card> getCardsOfType(Type t) {
        ArrayList<Card> cards = new ArrayList<>();

        for(Card c : Card.values()) {
            if(c.type == t) {
                cards.add(c);
            }
        }

        return cards;
    }

    /**
     * Get Playable cards of type
     */
    public static ArrayList<Card> getPlayableCards(Type t) {
        ArrayList<Card> cards = new ArrayList<>();

        for(Card c : Card.values()) {
            if(c.type == t && c != Card.CORRIDOR && c != Card.INACCESSIBLE) {
                cards.add(c);
            }
        }

        return cards;
    }


    /**
     * Get a card from your character, mainly for the serializer
     * @param character Singular character representing a card
     * @return Card enum.
     */
    public static Card getCardFromCharacter(String character) {
        for(Card card: Card.values()) {
            if(card.c.equals(character)) {
                return card;
            }
        }

        throw new RuntimeException("Invalid character! Can't find a card");
    }

    public void draw(Graphics g, int x, int y, int width, int height, String label) {
        this.draw(g, x, y, width, height);

        if(label != null) {
            int stringWidth = g.getFontMetrics().stringWidth(label);
            g.drawString(label, x + width / 2 - stringWidth / 2, y + height / 5);
        }
    }
}