package Model;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5092.1e2e91fc6 modeling language!*/

/**
 * Class representing an individual cell on the game board. Stores relevant information required for various parts of the game logic
 * 
 * @author Max
 *
 */

// line 47 "model.ump"
// line 135 "model.ump"
public class Cell
{

	//------------------------
	// MEMBER VARIABLES
	//------------------------

	//Cell Attributes
	private boolean isEntrance;
	private List<Player> occupied = new ArrayList<>();
	/**
	 * This stores the players that have died on this cell. it is non-blocking
	 */
	private List<Player> deadBodies = new ArrayList<>();

	private int row;
	private int col;

	//Cell Associations
	private Card room;

	//------------------------
	// CONSTRUCTOR
	//------------------------

	/**
	 * @param aIsEntrance Is this an place where we can change rooms
	 * @param aRow Row of the cell
	 * @param aCol Column of the cell
	 * @param aRoom What room is it a part of
	 * @author islas
	 */
	public Cell(boolean aIsEntrance, int aRow, int aCol, Card aRoom)
	{
		isEntrance = aIsEntrance;
		row = aRow;
		col = aCol;
		room = aRoom;
		if (aRoom.type != Card.Type.ROOM)
		{
			throw new RuntimeException("Unable to create Cell due to aRoom. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
	}

	//------------------------
	// INTERFACE
	//------------------------

	public boolean getIsEntrance()
	{
		return isEntrance;
	}

	public int getRow()
	{
		return row;
	}

	public int getCol()
	{
		return col;
	}
	/* Code from template association_GetOne */
	public Card getRoom()
	{
		return room;
	}

	// Returns string representation of the class
	// Checks if a character is in there.
	// then
	public String toString(){
		if(occupied.size() > 0) { // Check if there is a character on the cell which needs to be shown
			for(Player p : occupied) { // Search for an actual player first
				if(!p.isNpc()) {
					return p.toString();
				}
			}
			return occupied.get(0).toString(); // If no actual player on cell then print a random NPC
		}
		if(isEntrance) {
			return "^";
		}
		return room.toString(); // Otherwise return the rooms ASCII representation
	}

	public List<Player> getOccupied() {
		return occupied;
	}
	
	
	/**
	 * 
	 * Adds a player to the list. Will check whether or not 
	 * another player already exists and return false if failed to add.
	 * 
	 * NPCs are non blocking
	 * 
	 * @param p Player to add
	 * @return true if successfully added, false otherwise.
	 */
	public boolean addPlayer(Player p) {
		// Get players currently in the cell and try to add them
		if(this.hasRealPlayer()) {
			if(p.isNpc()) {
				this.occupied.add(p);
				return true;
			} else {
				return false;
			}
		} else {
			this.occupied.add(p);
			return true;
		}
	}


	/**
	 *
	 * Adds a player to the deadBodies list
	 *
	 */
	public void addDeadPlayer(Player p) {
		this.deadBodies.add(p);
	}



	/**
	 * Removes player from list
	 * 
	 * @param p Player
	 */
	public void removePlayer(Player p) {
		this.occupied.remove(p);
	}
	
	/**
	 * Checks if there is a non-NPC player on the cell
	 * @return true if exists
	 */
	public boolean hasRealPlayer() {
		for(Player occupier : this.getOccupied()) {
			if(!occupier.isNpc()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks if there is a NPC player on the cell
	 * @return true if exists
	 */
	public boolean hasNPCPlayer() {
		for(Player occupier : this.getOccupied()) {
			if(occupier.isNpc()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if there is a dead player on this cell
	 * @return true if it exists
	 */
	public boolean hasDeadPlayer() {
		return this.deadBodies.size() > 0;
	}

	/**
	 * Get the one real player in this cell
	 * @return Real human player on this cell
	 */
	public Player getRealPlayer() {
		for(Player occupier : this.getOccupied()) {
			if(!occupier.isNpc()) {
				return occupier;
			}
		}
		return null;
	}

	/**
	 * Get first NPC player on this cell.
	 * @return First NPC player in the list, else null;
	 */
	public Player getFirstNPCPlayer() {
		for(Player occupier : this.getOccupied()) {
			if(occupier.isNpc()) {
				return occupier;
			}
		}
		return null;
	}

	public static int cellWidth = 18;
	public static int cellHeight = 18;
	public static Color cellBorderColor = new Color(0,0,0, 0.4f);

	/**
	 * This function will be called by the relevant element. And it will draw it's rendering on the
	 * provided graphics (g) instance.
	 *
	 * @param g Graphics object to draw on
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param neighbourCells Cells next too this cell (for drawing walls)
	 */
	public void draw(Graphics g, int x, int y, HashMap<Board.Direction, Cell> neighbourCells){
		g.setColor(this.room.getColor());
		g.fillRect(x,y,cellWidth,cellHeight);

		if(this.getRoom() == Card.CORRIDOR) {
			g.setColor(cellBorderColor);
			g.drawRect(x,y, cellWidth, cellHeight);
		}

		if(this.hasRealPlayer()) {
			// Draw a player, first priority for drawing.
			this.getRealPlayer().draw(g, x, y);
		} else if(this.hasNPCPlayer()) {
			// If not, draw the NPC
			this.getFirstNPCPlayer().draw(g, x, y);
		} else if(this.hasDeadPlayer()) {
			// finally, draw the dead player
			this.deadBodies.get(0).draw(g, x, y);
		}

		g.setColor(Color.BLACK);
		Graphics2D g2 = (Graphics2D) g; // Get access to thickness methods
		g2.setStroke(new BasicStroke(2));

		// Print walls of the cells.
		for(Board.Direction d : neighbourCells.keySet()) {
			Cell neighbourCell = neighbourCells.get(d);
			boolean borderBetweenCorridorInaccessible = this.getRoom() == Card.CORRIDOR && neighbourCell.getRoom() == Card.INACCESSIBLE;

			if(neighbourCell.getRoom() != this.getRoom() &&
					(this.getRoom() != Card.INACCESSIBLE) &&
					!borderBetweenCorridorInaccessible &&
					(!neighbourCell.isEntrance || !this.isEntrance)) {
				// Rooms are different, we need to place a wall
				switch(d) {
					case LEFT:
						g2.drawLine(x, y, x, y + cellHeight);
						break;
					case RIGHT:
						g2.drawLine(x + cellWidth, y, x + cellWidth, y + cellHeight);
						break;
					case DOWN:
						g2.drawLine(x, y + cellHeight, x + cellWidth, y + cellHeight);
						break;
					case UP:
						g2.drawLine(x, y, x + cellWidth, y);
						break;
					default:
						break;
				}
			}
		}
		g2.setStroke(new BasicStroke(1));
	}

}