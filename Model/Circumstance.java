package Model;/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5099.60569f335 modeling language!*/



// line 101 "model.ump"
// line 169 "model.ump"
public class Circumstance
{

	//------------------------
	// MEMBER VARIABLES
	//------------------------

	//Circumstance Associations
	private Card character;
	private Card room;
	private Card weapon;

	//------------------------
	// CONSTRUCTOR
	//------------------------

	public Circumstance(Card aCharacter, Card aRoom, Card aWeapon)
	{
		character = aCharacter;
		room = aRoom;
		weapon = aWeapon;
		if (aCharacter.type != Card.Type.CHARACTER)
		{
			throw new RuntimeException("Unable to create Circumstance due to aCharacter. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
		if (aRoom.type != Card.Type.ROOM)
		{
			throw new RuntimeException("Unable to create Circumstance due to aRoom. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
		if (aWeapon.type != Card.Type.WEAPON)
		{
			throw new RuntimeException("Unable to create Circumstance due to aWeapon. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
	}

	//------------------------
	// INTERFACE
	//------------------------
	/* Code from template association_GetOne */
	public Card getCharacter()
	{
		return character;
	}
	/* Code from template association_GetOne */
	public Card getRoom()
	{
		return room;
	}
	/* Code from template association_GetOne */
	public Card getWeapon()
	{
		return weapon;
	}

	public String toString()
	{
		return character.toString() + "," + room.toString() + "," + weapon.toString();
	}

	public String getDescription() {
		return this.getCharacter().getName()+" in the "+this.getRoom().getName()+" with the "+this.getWeapon().getName();
	}

	// line 108 "model.ump"
	public boolean compareTo(Object o){
		if(o instanceof Circumstance) {
			// ensure all cards are the same.
			return this.character.ordinal() == ((Circumstance) o).getCharacter().ordinal() &&
					this.weapon.ordinal() == ((Circumstance) o).getWeapon().ordinal() &&
					this.room.ordinal() == ((Circumstance) o).getRoom().ordinal();
		} else {
			return false;
		}

	}

	// line 110 "model.ump"
	public boolean containsCard(Card c){
		return this.character == c ||
				this.weapon == c ||
				this.room == c;
	}

}