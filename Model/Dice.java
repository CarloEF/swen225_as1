package Model;

import java.awt.*;
import java.util.Random;

/**
 * Class representing a simple dice which gives a value between 1 and 6. Also used to draw the dice onto the GUI
 *
 * @author Max
 *
 */

public class Dice {
    private Random rand = new Random();
    private int value;

    public Dice(){
        value = rand.nextInt(6) + 1;
    }

    public int roll(){
        value = rand.nextInt(6) + 1;
        return value;
    }

    public void draw(Graphics g, int x, int y, int size){
        g.setColor(Color.BLACK);
        g.drawRect(x,y,size,size);
        if(value > 2){
            // Top Left
            g.fillOval(x + size / 12, y + size / 12, size / 6, size / 6);
        }
        if(value == 6){
            // Middle Left
            g.fillOval(x + size / 12, y + size * 5 / 12, size / 6, size / 6);
        }
        if(value == 2 || value > 3){
            // Bottom Left
            g.fillOval(x + size / 12, y + size * 9 / 12, size / 6, size / 6);
        }
        if(value == 1 || value == 3 || value == 5){
            // Middle
            g.fillOval(x + size * 5 / 12, y + size * 5 / 12, size / 6, size / 6);
        }
        if(value == 2 || value > 3){
            // Top Right
            g.fillOval(x + size * 9 / 12, y + size / 12, size / 6, size / 6);
        }
        if(value == 6){
            // Middle Right
            g.fillOval(x + size * 9 / 12, y + size * 5 / 12, size / 6, size / 6);
        }
        if(value > 2){
            // Bottom Right
            g.fillOval(x + size * 9 / 12, y + size * 9 / 12, size / 6, size / 6);
        }

    }

}