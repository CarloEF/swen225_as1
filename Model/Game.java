package Model;
import java.util.*;

/**
 * The Game class for Cluedo.
 * <p>
 * Contains the main() method, all game logic
 * and stores all the necessary high level objects (solution, players, gameBoard)
 *
 */
public class Game {

    //------------------------
    // MEMBER VARIABLES
    //------------------------

    //Game Attributes
    private Player currentPlayer;
    private Player currentRefuter;
    private Card cardRefuted;
    public Dice diceOne = new Dice();
    public Dice diceTwo = new Dice();

    private int movesRemaining = 0;


    //Game Associations
    private Circumstance solution;
    private Circumstance currentSuggestion;
    private Board gameBoard;
    private List<Player> players;


    /**
     * The game has modes in a turn, since functions are called
     * once when an action is done, it's important to know where the game state is at
     */
    public enum GamePhase {
        AWAIT_DICE("Click Dice to Roll."),
        MOVE("Move Character Using Arrow Keys."),
        GAME_OVER("Game Over!");

        String message;

        GamePhase(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private GamePhase gamePhase = GamePhase.AWAIT_DICE;

    //------------------------
    // CONSTRUCTOR
    //------------------------

    //
    public Game(Board aGameBoard, Player... allPlayers) {

        //  ------Creating all the cards, players, and hands------

        List<Card> allCharacters = Card.getPlayableCards(Card.Type.CHARACTER);
        List<Card> allWeapons = Card.getPlayableCards(Card.Type.WEAPON);
        List<Card> allRooms = Card.getPlayableCards(Card.Type.ROOM);

        players = new ArrayList<>();

        boolean didAddPlayers = setPlayers(allPlayers);
        if (!didAddPlayers) {
            throw new RuntimeException("Unable to create Game, must have 3 to 6 players. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }


        //	------Creating & setting the solution. Setting Game Board------
        Random rand = new Random();
        Card murderer = allCharacters.remove(rand.nextInt(allCharacters.size()));
        Card murderWeapon = allWeapons.remove(rand.nextInt(allWeapons.size()));
        Card murderRoom = allRooms.remove(rand.nextInt(allRooms.size()));
        Circumstance solution = new Circumstance(murderer, murderRoom, murderWeapon);

        if (!setSolution(solution)) {
            throw new RuntimeException("Unable to create Game due to solution. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }

        if (!setGameBoard(aGameBoard)) {
            throw new RuntimeException("Unable to create Game due to aGameBoard. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }


        //	------Putting the remaining cards in cardsToBeDealt------
        ArrayList<Card> cardsToBeDealt = new ArrayList<>();

        cardsToBeDealt.addAll(allCharacters);
        cardsToBeDealt.addAll(allWeapons);
        cardsToBeDealt.addAll(allRooms);


        //  ------Dealing out the cards to each player (in order of their turns)------
        int count = 0;
        while (cardsToBeDealt.size() > 0) {
            int randomIndex = rand.nextInt(cardsToBeDealt.size());
            currentPlayer = players.get(count % players.size());
            if (!currentPlayer.isNpc()) {
                Card cardToBeAdded = cardsToBeDealt.remove(randomIndex);
                currentPlayer.getHand().addCard(cardToBeAdded);
            }
            count++;
        }

        // Set the current player
        currentPlayer = players.get(5);
        // Get either player 1 or the first selected player

        setNextPlayer();
        currentRefuter = currentPlayer;
        currentSuggestion = new Circumstance(Card.MISS_SCARLETT, Card.BALLROOM, Card.CANDLESTICK);
    }

    /**
     * Takes a variable amount of arguments and sets it as a player list
     *
     * @param newPlayers Players to add to list
     * @return true on success.
     */
    public boolean setPlayers(Player... newPlayers) {
        ArrayList<Player> verifiedPlayers = new ArrayList<>();
        for (Player aPlayer : newPlayers) {
            if (verifiedPlayers.contains(aPlayer)) {
                continue;
            }
            verifiedPlayers.add(aPlayer);
        }

        if (verifiedPlayers.size() != newPlayers.length || verifiedPlayers.size() < minimumNumberOfPlayers() || verifiedPlayers.size() > maximumNumberOfPlayers()) {
            return false;
        }

        players.clear();
        players.addAll(verifiedPlayers);
        return true;
    }

    /**
     * Find the next playable character and set it.
     *
     * @return true on success.
     */
    public boolean setNextPlayer() {
        int currentPlayerIndex = players.indexOf(this.currentPlayer);

        for (int i = 1; i <= this.numberOfPlayers(); i++) {
            if (currentPlayerIndex < this.numberOfPlayers() - 1) {
                currentPlayerIndex += 1;
            } else {
                currentPlayerIndex = 0;
            }

            Player nextPlayer = this.getPlayer(currentPlayerIndex);
            if (!nextPlayer.isDead() && !nextPlayer.isNpc()) {
                // Player is not dead!
                this.setCurrentPlayer(nextPlayer);
                return true;
            }
        }
        return false;
    }


    /**
     * Go to the next turn, set to the appropriate player.
     * Etc.
     *
     * Returns if "false" if the game is over
     */
    public void nextTurn() {
        if (!this.canEndTurn()) {
            throw new RuntimeException("Can't end turn due.");
        }

        // Erase the travelled cells
        this.currentPlayer.lastTravelledCells.clear();

        // Try set next player
        if (setNextPlayer()) { // If true then currentPlayer has been updated
            this.setGamePhase(GamePhase.AWAIT_DICE);
            return;
        }

        // All players are dead
        this.setGamePhase(GamePhase.GAME_OVER);
    }

    /**
     * Get the player associated from character.
     * @return Player p
     */
    public Player getPlayer(Card c) {
        for(Player p : this.players) {
            if(p.getCharacter() == c) {
                return p;
            }
        }

        throw new RuntimeException("No player associated with this card!");
    }

    public boolean canEndTurn() {
        return this.getGamePhase() == GamePhase.MOVE && this.getMovesRemaining() == 0;
    }


    /**
     * Creates a new game based on the amount of players playing.
     *
     * @return Game
     */
    public static Game initialiseNewGame(String[] names, Integer[] characters) {
        Serializer serializer = new Serializer();
        serializer.generateDefaultGame();
        Board newBoard = serializer.getBoard();

        Player[] players = new Player[6];

        for (int i = 0; i < 6; i++) {

            Player p = serializer.getPlayers().get(i);
            boolean characterChosen = false;
            String name = "";
            for (int j = 0; j < 6; j++) { // Need to check if any of the players chosen the character in question
                characterChosen = characterChosen || p.getCharacter().toString().equals(String.valueOf(characters[j]));
                if (p.getCharacter().toString().equals(String.valueOf(characters[j]))) {
                    name = names[j];
                }
            }
            if (characterChosen) { // If so then record their name
                p.setName(name);
            } else {
                p.setNpc(true); // If not then set character to npc
            }
            players[i] = p;
        }
        Game g = new Game(newBoard, players);
        g.getGameBoard().initialiseStartPoints();
        return g;
    }

    /**
     * Uses the provided serializer class to generate a new game instance!
     *
     * @param serializer Serializer to turn into a game
     * @return Created game class!
     */
    public static Game loadGame(Serializer serializer) {
        Serializer blank = new Serializer();
        blank.generateDefaultGame();
        Player[] players = new Player[6];
        int j = 0;
        for (Player p : blank.getPlayers()) {
            players[j++] = p;
        }
        Game game = new Game(blank.getBoard(), players);
        j = 0;
        Player[] players2 = new Player[6];
        for (Player p : serializer.getPlayers()) {
            players2[j++] = p;
        }
        game.setPlayers(players2);
        game.setGameBoard(serializer.getBoard());
        game.setSolution(serializer.getSolution());
        game.setCurrentPlayer(serializer.getCurrentPlayer());
        game.getGameBoard().initialiseStartPoints();
        return game;
    }


    /**
     * Check if a circumstance can be refuted by other players.
     * This function takes a suggestion, then will go through each
     * Player until one player can refute the suggestion. When that happens
     * we will get that card from the player and return it.
     *
     * If no card can refute the suggestion we will return null.
     *
     * @return Card that was used to refute.
     */
    public boolean checkForRefutation() {
        currentPlayer.setLastRoomSuggested(currentPlayer.getCurrentCell().getRoom());

        int playerNumber = this.getPlayers().indexOf(this.getCurrentPlayer()); // What number is current player.

        for (int i = 1; i < this.numberOfPlayers(); i++) {
            int playerToCheck = (i + playerNumber) % this.numberOfPlayers();

            Player p = this.getPlayer(playerToCheck);

            if (p.isNpc()) { // No need for NPCs to refute, they don't have hands.
                continue;
            }

            // If they can refute, return true
            if (p.canRefute(currentSuggestion)) {
                currentRefuter = p;
                return true;
            }
        }
        currentRefuter = null;
        return false;
    }

    /**
     * Rolls two dice.
     */
    public void rollDice() {
        movesRemaining = diceOne.roll() + diceTwo.roll();
    }

    /**
     * Auto-generated toString method for Game.
     */
    public String toString() {
        return
                "currentPlayer = " + currentPlayer.toString() +
                        "\nplayers = " + players.toString() +
                "\nsolution = " + solution.toString() +
                "\ngameBoard = " + gameBoard.toString();
    }


    /* ----- Getters and Setters ---- */
    public Player getCurrentRefuter() { return currentRefuter; }

    public void setCardRefuted(Card c) { this.cardRefuted = c; }

    public Card getCardRefuted() { return cardRefuted; }

    public Circumstance getCurrentSuggestion() { return currentSuggestion; }

    public void setCurrentSuggestion(Circumstance currentSuggestion) { this.currentSuggestion = currentSuggestion; }

    public int getMovesRemaining() { return movesRemaining; }

    public void setMovesRemaining(int movesRemaining) { this.movesRemaining = movesRemaining; }

    public GamePhase getGamePhase() { return gamePhase; }

    public void setGamePhase(GamePhase gamePhase) { this.gamePhase = gamePhase; }

    public Player getCurrentPlayer() { return currentPlayer; }

    public Circumstance getSolution() { return solution; }

    public Board getGameBoard() { return gameBoard; }

    public Player getPlayer(int index) { return players.get(index); }

    public List<Player> getPlayers() { return Collections.unmodifiableList(players); }

    public int numberOfPlayers() { return players.size(); }

    public boolean setSolution(Circumstance aNewSolution) {
        solution = aNewSolution;
        return true;
    }

    public boolean setGameBoard(Board aNewGameBoard) {
        boolean wasSet = false;
        if (aNewGameBoard != null) {
            gameBoard = aNewGameBoard;
            wasSet = true;
        }
        return wasSet;
    }

    public static int minimumNumberOfPlayers() { return 3; }

    public static int maximumNumberOfPlayers() { return 6; }

    public void setCurrentPlayer(Player aCurrentPlayer) { currentPlayer = aCurrentPlayer; }

    public boolean compareTo(Game otherGame){
        return this.toString().equals(otherGame.toString());
    }

    public boolean checkAllDead(){
        boolean someoneAlive = false;
        for(Player p : players){ // Look for a player who is still alive
            someoneAlive = someoneAlive || (!p.isNpc() && !p.isDead()); // Only need to find one player who is not an npc and not dead
        }
        return !someoneAlive;
    }

}