package Model;

import java.awt.*;
import java.util.*;
import java.util.List;

/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5099.60569f335 modeling language!*/

/**
 * Class representing a hand of cards belonging to a particular Player. Used to refute other players suggestions.
 * 
 * @author Max
 *
 */

// line 93 "model.ump"
// line 159 "model.ump"
public class Hand
{

	//------------------------
	// MEMBER VARIABLES
	//------------------------

	private List<Card> cards;

	public Hand()
	{
		cards = new ArrayList<>();
	}
	
	public String toString() {
		if(cards.size() == 0) { return ""; }
		StringBuilder sB = new StringBuilder();
		sB.append(cards.get(0).toString());
		for(int i = 1; i < cards.size(); i++) {
			sB.append("-").append(cards.get(i).toString());
		}
		return sB.toString();
	}

	/**
	 * Is card in this person's hand.
	 * 
	 * @param c Card to check
	 * @return true if within hand.
	 */
	public boolean containsCard(Card c){
		return this.cards.contains(c);
	}


	public void draw(Graphics g, int width, int height, Card selectedCard, Circumstance suggestion){
		int cardWidth = (int) (width * 0.3);
		int cardHeight = (int) (height * 0.45);
		for(int i = 0; i < 3; i++){
			Card c = cards.get(i);
			if(c.equals(selectedCard)){
				c.highlight(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.025),cardWidth, cardHeight);
			} else if(suggestion.containsCard(c)){
				c.draw(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.025),cardWidth, cardHeight);
			} else {
				c.drawFaded(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.025),cardWidth, cardHeight);
			}
		}
		for(int i = 0; i < (cards.size() - 3); i++){
			Card c = cards.get(i + 3);
			if(c.equals(selectedCard)){
				c.highlight(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.525),cardWidth, cardHeight);
			} else if(suggestion.containsCard(c)){
				c.draw(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.525),cardWidth, cardHeight);
			} else {
				c.drawFaded(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.525),cardWidth, cardHeight);
			}
		}
	}

	public void draw(Graphics g, int width, int height, Card selectedCard){
		int cardWidth = (int) (width * 0.3);
		int cardHeight = (int) (height * 0.45);
		for(int i = 0; i < 3; i++){
			if(cards.get(i).equals(selectedCard)){
				cards.get(i).highlight(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.025),cardWidth, cardHeight);
			} else {
				cards.get(i).draw(g, (int) ((i * 0.325 + 0.025) * width),(int) (height * 0.025),cardWidth, cardHeight);
			}
		}
		for(int i = 0; i < (cards.size() - 3); i++){
			if(cards.get(i + 3) == selectedCard){
				cards.get(i + 3).highlight(g, (int) ((i * 0.325 + 0.025) * width), (int) (height * 0.525), cardWidth, cardHeight);
			} else {
				cards.get(i + 3).draw(g, (int) ((i * 0.325 + 0.025) * width), (int) (height * 0.525), cardWidth, cardHeight);
			}
		}

	}

	/* Getters and Setters */

	public Card getCard(int index)
	{
		if(index >= cards.size() || index < 0){ return null;}
		return cards.get(index);
	}

	public List<Card> getCards()
	{
		return cards;
	}

	public void addCard(Card aCard)
	{
		if (cards.contains(aCard)) { return; }
		cards.add(aCard);
	}

	public String getDescription() {
		if(cards.size() == 0) { return "{}"; }
		StringBuilder sB = new StringBuilder();
		sB.append("{").append(cards.get(0).getDescription());
		for(int i = 1; i < cards.size(); i++) {
			sB.append(" - ").append(cards.get(i).getDescription());
		}
		sB.append("}");
		return sB.toString();
	}
}