package Model;

import java.awt.*;
import java.util.ArrayList;




/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5099.60569f335 modeling language!*/



// line 57 "model.ump"
// line 139 "model.ump"
public class Player
{

	//------------------------
	// MEMBER VARIABLES
	//------------------------

	//Player Attributes
	private boolean isDead;
	/**
	 * Custom player name set in PlayerDetails GI
	 */
	private String name;

	//Player Associations
	private Card character;
	private boolean isNpc;
	private boolean canAccuse;
	private Cell currentCell;
	
	private Hand hand;
	private Card lastRoomSuggested;

	/**
	 * New variable. To facilitate new movement functions, we must move away from our
	 * semi-procedural style of programming.
	 *
	 * This variable will store the cells the player has moved to in this current turn
	 * and will be erased at the end of it.
	 */
	public ArrayList<Cell> lastTravelledCells = new ArrayList<>();

	//------------------------
	// CONSTRUCTOR
	//------------------------

	public Player(Card aCharacter, Hand aHand)
	{
		isDead = false;
		setCharacter(aCharacter);
		if (aCharacter.type != Card.Type.CHARACTER)
		{
			throw new RuntimeException("Unable to create player due to character. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
		if (!setHand(aHand))
		{
			throw new RuntimeException("Unable to create Player due to aHand. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
	}

	public Player(Card aCharacter, Hand aHand, boolean isNPC)
	{
		isDead = false;
		boolean didAddCharacter = setCharacter(aCharacter);
		if (!didAddCharacter)
		{
			throw new RuntimeException("Unable to create player due to character. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
		if (!setHand(aHand))
		{
			throw new RuntimeException("Unable to create Player due to aHand. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
		}
		this.setNpc(isNPC);
	}

	
	//------------------------
	// INTERFACE
	//------------------------

	public void setIsDead(boolean aIsDead)
	{
		isDead = aIsDead;
	}

	public boolean isDead()
	{
		return isDead;
	}
	/* Code from template association_GetOne */
	public Card getCharacter()
	{
		return character;
	}
	/* Code from template association_GetOne */
	public Hand getHand()
	{
		return hand;
	}
	/* Code from template association_GetOne */
	public Card getLastRoomSuggested()
	{
		return lastRoomSuggested;
	}

	public boolean hasLastRoomSuggested()
	{
		return lastRoomSuggested != null;
	}
	
	/* Code from template association_SetOneToOptionalOne */
	public boolean setCharacter(Card aNewCharacter)
	{
		if(aNewCharacter.type != Card.Type.CHARACTER) {
			throw new RuntimeException("Card is not a character card!");
		}
		this.character = aNewCharacter;
		return true;
	}
		
	/* Code from template association_SetUnidirectionalOne */
	public boolean setHand(Hand aNewHand)
	{
		boolean wasSet = false;
		if (aNewHand != null)
		{
			hand = aNewHand;
			wasSet = true;
		}
		return wasSet;
	}
	/* Code from template association_SetUnidirectionalOptionalOne */
	public void setLastRoomSuggested(Card aNewLastRoomSuggested)
	{
		if(aNewLastRoomSuggested.type != Card.Type.ROOM) {
			throw new RuntimeException("Card type is not a Room card!");
		}

		lastRoomSuggested = aNewLastRoomSuggested;
	}

	/**
	 * 
	 * Can this player refute a given suggestion?
	 * 
	 * @param suggestion Suggestion to check
	 * @return true if one card can refute
	 */
	public boolean canRefute(Circumstance suggestion) {
		// See if a hand has the cards necessary to refute, return boolean.
		// compare each suggestion item.
		for(Card c : this.getHand().getCards()) {
			if(suggestion.containsCard(c)) {
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		return this.character.toString();
	}
	
	/**
	 * 
	 * Can the player suggest? Will check to see if the last room we suggested in is not equal to this room.
	 * The last room suggested boolean is set to null each time the player moves out of a room.
	 * 
	 * @return true if player can suggest.
	 */
	public boolean canSuggest() {
		// To implement
		if(this.isNpc()) return false;
		
	
		// If the last room we suggested (it will be non-null if we suggested recently)
		// is the same as the room we're currently in, don't let the player suggest.
		// Otherwise, ensure we are in a room and not in the corridor
		if(this.getLastRoomSuggested() == this.getCurrentCell().getRoom()) {
			return false;
		} else {
			return this.getCurrentCell().getRoom() != Card.CORRIDOR && this.getCurrentCell().getRoom() != Card.INACCESSIBLE;
		}
	}

	public boolean isNpc() {
		return isNpc;
	}

	public void setNpc(boolean isNpc) {
		this.isNpc = isNpc;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public Cell getCurrentCell() {
		return currentCell;
	}

	public void setCurrentCell(Cell currentCell) {
		this.currentCell = currentCell;
	}

	public boolean canAccuse() {
		return canAccuse && !this.isDead() && this.getCurrentCell().getRoom() != Card.CORRIDOR && this.getCurrentCell().getRoom() != Card.INACCESSIBLE;
	}

	public void setCanAccuse(boolean canAccuse) {
		this.canAccuse = canAccuse;
	}
	
	/**
	 * Move the player to the new cell, does basic check to ensure there is no invalid game state
	 * Validity of move is determined by the Board class.
	 * 
	 * @param destination Cell to move to
	 * @return true if successful, false if not.
	 */
	public boolean move(Cell destination) {
		// Check this is an adjacent move by exactly one cell
		if(destination.hasRealPlayer()) return false;
				
		this.currentCell.removePlayer(this);
		this.currentCell = destination;
		destination.addPlayer(this);
		return true;
	}

	public void draw(Graphics g, int x, int y) {
		g.setColor(this.getCharacter().color);
		g.fillOval(x + 1, y + 1 , Cell.cellWidth - 2, Cell.cellHeight - 2);
		g.setColor(Color.BLACK);
		g.drawOval(x + 1 , y + 1, Cell.cellWidth - 2, Cell.cellHeight - 2);

		if(this.isDead()) {
			// Player is dead, render dead
			g.setColor(Color.RED);
			g.drawLine(x, y, x + Cell.cellWidth, y + Cell.cellHeight);
			g.drawLine(x + Cell.cellWidth, y, x, y + Cell.cellHeight);
		}
	}

	/**
	 * This kills the player and does what's necessary to facilitate it
	 * Leaves dead body on cell etc.
	 */
	public void killPlayer() {
		this.isDead = true;

		this.currentCell.removePlayer(this);
		this.currentCell.addDeadPlayer(this);

		this.canAccuse = false;
	}
}