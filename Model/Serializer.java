package Model;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.0.5092.1e2e91fc6 modeling language!*/

/**
 * Class representing a serializer used to convert between a playable game state and a single text string used for load / save methods as well as initialising the game.
 *
 * @author Max
 *
 */

// line 113 "model.ump"
// line 175 "model.ump"
public class Serializer
{

    private Circumstance solution;
    private Board board;
    private List<Player> players;
    private Player currentPlayer = null;

    //------------------------
    // CONSTRUCTOR
    //------------------------

    public Serializer()
    {
        this.solution = null;
        this.board = new Board();
        this.players = new ArrayList<>();
    }

    //------------------------
    // INTERFACE
    //------------------------

    /**
     * The overall generate game method which breaks down the input string into the various parts of the game (Board, Players, Solution).
     *
     * @author Max
     *
     */

    public Boolean loadFromFile(File f){
        try{
            Scanner fileScan = new Scanner(f);
            String loadString = fileScan.nextLine();
            return generateGame(loadString);
        } catch (Exception e){
            return false;
        }
    }

    public Boolean generateGame(String gameStr) {
        if(gameStr.length() < 1210) { return false; }
        try {
            generateBoard(gameStr); // The first 1200 characters determine the rooms and entrance status of each cell
            generateCircumstance(gameStr.substring(1200, 1205)); // A small 5 character long string that provides the murder circumstance
            generatePlayer(gameStr.substring(1207)); // A varying length of string at the end of the saveString denoting information about each player
            this.currentPlayer = this.players.get(Integer.parseInt(gameStr.substring(1206, 1207)) - 1); // A single number (1-6) denoting which player's  turn it is
            return this.solution != null && this.board != null && this.players != null && this.currentPlayer != null;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * The overall generate game method which breaks down the input string into the various parts of the game (Board, Players, Solution).
     *
     * @author Max
     *
     */
    public void generateDefaultGame()
    {
        String defaultGameString = "*********_****_*********kkkkkk*___bbbb___*cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb__cccccckkkkkk__bbbbbbbb___cccc**kkkkk__bbbbbbbb________________bbbbbbbb_______**_________________BBBBBBddddd_____________BBBBBBdddddddd__*****___BBBBBBdddddddd__*****___BBBBBBdddddddd__*****___BBBBBBdddddddd__*****________*dddddddd__*****___lllll*dddddddd__*****__lllllll*_________*****__lllllll_________________lllllll*________hhhhhh___lllll*LLLLLLL__hhhhhh_________LLLLLLL__hhhhhh________*LLLLLLL__hhhhhh__sssssssLLLLLLL__hhhhhh__sssssssLLLLLLL__hhhhhh__sssssssLLLLLL*_*hhhhhh*_*ssssss0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000110000001101000000000100000000000000000000000100001000010000000000000000001000010000000000000000000000000011000000000000000000000000000000000000000000000000000000000000110000000000000100000000000000000000010100000000000000000000010000000001000000000000000000000001000000000110000000000000000011000000000000000001000011000000000000000001000000000000000000000000000000011010000000000000000000000010000000000000000000000000000000000000000000000000000000000000000000000000000001,k,U,1,1,0,0,0,24,7,,*,A,2,0,0,0,17,0,,*,B,3,0,0,0,0,9,,*,C,4,0,0,0,0,14,,*,D,5,0,0,0,6,23,,*,E,6,0,0,0,19,23,,*,F";
        generateGame(defaultGameString); // The first 1200 characters determine the rooms and entrance status of each cell.
    }

    /**
     * The method for generating a board of cells.
     *
     * @author Max
     *
     */
    public void generateBoard(String boardString)
    {
        this.board = new Board();
        for(int row = 0; row < Board.ROWS; row++) {
            for(int col = 0; col < Board.COLUMNS; col++) {
                Cell c = generateCell(boardString.charAt(Board.COLUMNS * row + col), row, col, boardString.charAt(Board.COLUMNS * row + col + 600));
                if(c == null) {
                    this.board = null;
                    return;
                }
                this.board.addCellAt(c, Board.COLUMNS * row + col);
            }
        }
    }

    /**
     * The method for generating a single cell using information about its row, column, the room its in and whether or not it is an entrance
     *
     * @author Max
     *
     */
    public Cell generateCell(char room, int row, int col, char entrance) {
        Card r = generateRoom(String.valueOf(room));
        if(r == null) {
            return null;
        }
        boolean isEntrance = entrance == '1';
        return new Cell(isEntrance, row, col, r);
    }

    /**
     * The method for generating a circumstance. Is used to generate the murder solution
     *
     * @author Max
     *
     */
    public void generateCircumstance(String solutionString) { // Generate the saved solution from the character, room and weapon
        try {
            Scanner scan = new Scanner(solutionString);
            scan.useDelimiter(",");
            Card murderer = generateCharacter(scan.next());
            Card murderScene = generateRoom(scan.next());
            Card murderWeapon = generateWeapon(scan.next());
            this.solution = new Circumstance(murderer, murderScene, murderWeapon);
            scan.close();
        } catch(Exception e) {
            // Catch exception in case of invalid input
            this.solution = null;
        }

    }

    /**
     * The method for generating a player using information about the cell its on, the cards in its hand and whether or not it is an npc (Non-player Character).
     *
     * @author Max
     *
     */
    public void generatePlayer(String playerString) {
        try {
            Scanner scan = new Scanner(playerString);
            scan.useDelimiter(",");
            while(scan.hasNext()) {
                String playerNum = scan.next(); // Player number
                boolean isNpc = scan.nextInt() == 1; // isNpc
                boolean isDead = scan.nextInt() == 1; // isNpc
                boolean canAccuse = scan.nextInt() == 1; // isNpc
                int row = scan.nextInt(); // Row
                int col = scan.nextInt(); // Col
                Hand playerHand = generateHand(scan.next()); // Hand
                Card lastRoomSuggested = generateRoom(scan.next()); // Last Room
                Cell cell = this.board.getCell(Board.COLUMNS * row + col);
                Card c = generateCharacter(playerNum);
                String name = scan.next();

                Player p = new Player(c, playerHand);

                p.setHand(playerHand);
                p.setNpc(isNpc);
                p.setCanAccuse(canAccuse);
                p.setIsDead(isDead);
                p.setCurrentCell(cell);
                p.setLastRoomSuggested(lastRoomSuggested);
                p.setName(name);

                cell.addPlayer(p);
                this.players.add(p);
            }
            scan.close();
        } catch(Exception e) {
            // Catch exception in case of invalid input
            this.players = null;
        }
    }

    /**
     * The method for generating a hand of cards
     *
     * @author Max
     *
     */
    public Hand generateHand(String handString) {
        Scanner handContents = new Scanner(handString); // Hand
        handContents.useDelimiter("-");
        Pattern characterKeys = Pattern.compile("[1-6]");
        Pattern weaponKeys = Pattern.compile("[U-Z]");
        Hand playerHand = new Hand();
        try {
            while(handContents.hasNext()) {
                if(handContents.hasNext(characterKeys)) {
                    playerHand.addCard(generateCharacter(handContents.next()));
                } else if(handContents.hasNext(weaponKeys)) {
                    playerHand.addCard(generateWeapon(handContents.next()));
                } else {
                    playerHand.addCard(generateRoom(handContents.next()));
                }
            }
        } catch(Exception e) {
            // Catch exception in case of invalid input
            handContents.close();
            return null;
        }
        handContents.close();
        return playerHand;
    }

    /**
     * The method for generating a Character
     *
     * @author Max
     *
     */
    public Card generateCharacter(String cardString) {
        return Card.getCardFromCharacter(cardString);
    }

    /**
     * The method for generating a Room
     *
     * @author Max
     *
     */
    public Card generateRoom(String roomString) {
        return Card.getCardFromCharacter(roomString);
    }

    /**
     * The method for generating a Weapon
     *
     * @author Max
     *
     */
    public Card generateWeapon(String cardString) {
        return Card.getCardFromCharacter(cardString);
    }

    /**
     * The method for 'saving the game' aka converting all crucial information about the game into a single text string.
     *
     * @author Max
     *
     */
    public String saveGame(Game gameState) {
        StringBuilder saveString = new StringBuilder();
        // Acquire all the relevant data from the game
        this.board = gameState.getGameBoard();
        this.solution = gameState.getSolution();
        this.currentPlayer = gameState.getCurrentPlayer();
        this.players = gameState.getPlayers();

        // Iterate through the board twice, first taking the rooms and then the location of entrances
        for(int i = 0; i < 600; i++) {
            saveString.append(this.board.getCell(i).getRoom().toString());
        }
        for(int i = 0; i < 600; i++) {
            saveString.append(this.board.getCell(i).getIsEntrance() ? "1" : "0");
        }

        // Attach more guaranteed information such as the solution and which players turn it is.
        saveString.append(this.solution.toString()).append(",");
        saveString.append(this.currentPlayer.toString()).append(",");

        // For each player we need to save their number, whether or not a player controls them, the row and column they are in, the contents of their hand and the last room suggested
        for(int i = 0; i < 6; i++) {
            Player p = this.players.get(i);
            String playerNum = p.toString();
            String isNpc = p.isNpc() ? "1" : "0";
            String isDead = p.isDead() ? "1" : "0";
            String canAccuse = p.canAccuse() ? "1" : "0";
            int row = p.getCurrentCell().getRow();
            int col = p.getCurrentCell().getCol();
            String hand = p.getHand().toString();
            String lastRoom;
            if(p.hasLastRoomSuggested()) {
                lastRoom = p.getLastRoomSuggested().toString();
            } else {
                lastRoom = "*";
            }
            String name = p.getName();
            saveString.append(playerNum).append(",").append(isNpc).append(",").append(isDead).append(",").append(canAccuse).append(",").append(row).append(",").append(col).append(",").append(hand).append(",").append(lastRoom).append(",").append(name);
            if(i < 5) { saveString.append(","); }
        }
        return saveString.toString();
    }

    public Circumstance getSolution() {
        return this.solution;
    }

    public Board getBoard() {
        return this.board;
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }

}