package View;

import Model.Game;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

import static View.GUI.DEFAULT_FRAME_HEIGHT;
import static View.GUI.DEFAULT_FRAME_WIDTH;

/**
 * AbstractPanel class.
 *
 * Our View objects extend this base and override the editBody, editFooter etc. functions to
 * create a different design.
 *
 */
public abstract class AbstractPanel extends JPanel {
    protected GUI theView;
    protected Game theModel;
    protected JPanel contents;
    protected JPanel header;
    protected JPanel topHeader = new JPanel();
    protected JPanel bottomHeader = new JPanel();
    protected JPanel body;
    protected JPanel footer;
    protected DrawingBoard drawing;

    protected Border lineBorder = BorderFactory.createLineBorder(Color.BLACK, 2);
    protected Border padding = BorderFactory.createEmptyBorder(2,2,2,2);
    protected Border genericBorder = BorderFactory.createCompoundBorder(padding, lineBorder);

    /**
     * Constructor
     * @param view GUI base class
     * @param model Game.
     */
    public AbstractPanel(GUI view, Game model) {
        theView = view;
        theModel = model;
        initialise();
        editTopHeader();
        editBottomHeader();
        editBody();
        editFooter();
    }

    /**
     * Initialise the layout boxes (top header, bottom header, etc.)
     */
    protected void initialise(){

        header = new JPanel();
        header.setLayout(new BoxLayout(header, BoxLayout.PAGE_AXIS));
        header.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, (int) (DEFAULT_FRAME_HEIGHT * 0.2)));
        header.setBorder(genericBorder);
        header.setVisible(true);

        topHeader.setVisible(true);
        bottomHeader.setVisible(true);

        body = new JPanel();
        body.setBorder(genericBorder);
        body.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, (int) (DEFAULT_FRAME_HEIGHT * 0.6)));
        body.setVisible(true);

        footer = new JPanel();
        footer.setBorder(genericBorder);
        footer.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, (int) (DEFAULT_FRAME_HEIGHT * 0.2)));
        footer.setVisible(true);
    }


    /**
     * Create a blank JPanel for addition elsewhere
     * @return Created JPane;
     */
    public JPanel constructPanel(){
        contents = new JPanel(new BorderLayout());
        contents.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));

        header.add(topHeader);
        header.add(bottomHeader);

        contents.add(BorderLayout.NORTH, header);
        // TODO Scrollbody?
        JScrollPane scrollBody = new JScrollPane(body,  JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        contents.add(BorderLayout.CENTER, body);
        contents.add(BorderLayout.SOUTH, footer);

        return this.contents;
    }


    /**
     * Drawing board subclass. Used for anything where we need to draw custom objects. E.g. the game board.
     */
    public class DrawingBoard extends JComponent{
        public DrawingBoard(){
            this.setPreferredSize(new Dimension((int) (DEFAULT_FRAME_WIDTH * 0.9), (int) (DEFAULT_FRAME_HEIGHT * 0.5)));
            this.setBorder(genericBorder);
            this.setVisible(true);
        }
    }


    /**
     * Edit the top heder (just below the nav bar)
     */
    protected abstract void editTopHeader();


    /**
     * Edit just the header just below the top header.
     */
    protected abstract void editBottomHeader();


    /**
     * Edit the body of the window
     */
    protected abstract void editBody();


    /**
     * Edit the footer of the window.
     */
    protected abstract void editFooter();

}
