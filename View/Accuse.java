package View;

import Model.*;

import javax.swing.*;

import static View.GUI.DEFAULT_FRAME_WIDTH;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class  Accuse extends AbstractPanel {

    JButton submit;
    private Card room;
    private Card character;
    private Card weapon;
    private Circumstance accusation;

    public Accuse(GUI view, Game model){
        super(view, model);
    }

    @Override
    protected void editTopHeader() {

    }

    @Override
    protected void editBottomHeader() {
        submit = new JButton("Submit");
        bottomHeader.add(submit);
        submit.setEnabled(false);
        submit.setVisible(true);
    }


    private void createAccusationPanel(JPanel accusation){
        accusation.setLayout(new BoxLayout(accusation, BoxLayout.Y_AXIS));
        accusation.setBorder(genericBorder);
    }

    @Override
    protected void editBody() {
        body.add(BorderLayout.NORTH, new JLabel("What is your accusation?..."));
        //Room Selection
        JPanel characterAccuse = new JPanel();
        characterAccuse.add(new JLabel("Character"));
        createAccusationPanel(characterAccuse);

        JPanel weaponAccuse = new JPanel();
        weaponAccuse.add(new JLabel("Weapon"));
        createAccusationPanel(weaponAccuse);

        JPanel roomAccuse = new JPanel();
        roomAccuse.add(new JLabel("Room"));
        createAccusationPanel(roomAccuse);

        //Setup Radio Buttons for each room
        ArrayList<Card> roomCards = Card.getPlayableCards(Card.Type.ROOM);
        ArrayList<Card> weaponCards = Card.getPlayableCards(Card.Type.WEAPON);
        ArrayList<Card> characterCards = Card.getPlayableCards(Card.Type.CHARACTER);

        // This action listener will be assigned to each character
        ActionListener setAccusation = actionEvent -> {
            Card c = Card.getCardFromCharacter(actionEvent.getActionCommand());
            if (c.getType() == Card.Type.ROOM){ room = c; }
            else if (c.getType() == Card.Type.WEAPON){ weapon = c; }
            else if (c.getType() == Card.Type.CHARACTER){ character = c; }

            if (room != null && weapon != null && character != null){
                submit.setEnabled(true);
                accusation = new Circumstance(character, room, weapon);
            }

            else { submit.setEnabled(false); }
        };

        // ---- Create room radio buttons
        JRadioButton[] roomButtons = new JRadioButton[roomCards.size()];
        ButtonGroup roomGroup = new ButtonGroup();

        for(int i = 0; i<roomCards.size(); i++) {
            roomButtons[i] = new JRadioButton(roomCards.get(i).getDescription());
            roomButtons[i].addActionListener(setAccusation);
            roomButtons[i].setActionCommand(roomCards.get(i).toString());
            roomGroup.add(roomButtons[i]);
        }

        // --- Create weapon radio buttons.
        JRadioButton[] weaponButtons = new JRadioButton[weaponCards.size()];
        ButtonGroup weaponGroup = new ButtonGroup();

        for(int i = 0; i<weaponCards.size(); i++) {
            weaponButtons[i] = new JRadioButton(weaponCards.get(i).getDescription());
            weaponButtons[i].addActionListener(setAccusation);
            weaponButtons[i].setActionCommand(weaponCards.get(i).toString());
            weaponGroup.add(weaponButtons[i]);
        }

        // -- Create character radio buttons.
        JRadioButton[] characterButtons = new JRadioButton[characterCards.size()];
        ButtonGroup characterGroup = new ButtonGroup();

        for(int i = 0; i<characterCards.size(); i++) {
            characterButtons[i] = new JRadioButton(characterCards.get(i).getDescription());
            characterButtons[i].addActionListener(setAccusation);
            characterButtons[i].setActionCommand(characterCards.get(i).toString());
            characterGroup.add(characterButtons[i]);
        }

        // Add to panels
        for(JRadioButton b :roomButtons)
            roomAccuse.add(b);
        roomAccuse.setVisible(true);
        body.add(roomAccuse);

        for(JRadioButton b :weaponButtons)
            weaponAccuse.add(b);
        weaponAccuse.setVisible(true);
        body.add(weaponAccuse);

        for(JRadioButton b :characterButtons)
            characterAccuse.add(b);
        characterAccuse.setVisible(true);
        body.add(characterAccuse);
    }

    @Override
    protected void editFooter() {
        JComponent handPanel = new JComponent() {
            protected void paintComponent(Graphics g) {
                Player currentPlayer = theModel.getCurrentPlayer();
                Hand h = currentPlayer.getHand();
                h.draw(g, (int) (this.getPreferredSize().width * 0.9), (int) (this.getPreferredSize().height * 0.8), null);
            }
        };

        handPanel.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, (int) footer.getPreferredSize().getHeight()));
        handPanel.setVisible(true);

        footer.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2), "Your Hand"));
        footer.add(handPanel);
    }

    public Circumstance getAccusation() {
        return accusation;
    }

    public void addSubmitListener(ActionListener aL){
        submit.addActionListener(aL);
    }

}