package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GUI extends JFrame{

    // Default Height and Width of the Frame, feel free to change
    public static final int DEFAULT_FRAME_HEIGHT = 800;
    public static final int DEFAULT_FRAME_WIDTH = 750;

    /**
     * The Menu items that are present on every game screen.
     */

    public JMenuBar menuBar;
    public JMenuItem newGameButton;
    public JMenuItem loadGameButton;
    public JMenuItem saveGameButton;
    public JMenuItem quitToMenu;
    public JMenuItem quitGame;

    public JPanel contents = new JPanel();

    /**
     * The views that make up each Game screen.
     */

    private Accuse accuse;
    private LoadGame loadGame;
    private Main main;
    private NewGame newGame;
    private Refute refute;
    private Result result;
    private Suggest suggest;
    private Welcome welcome;

    public static CardLayout cL = new CardLayout();

    public GUI(){
        setTitle("Assignment 2: Cluedo");
        setSize(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        initialise();

        contents.setLayout(cL);
        contents.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));

        add(contents);

        pack();
        setVisible(true);
    }

    public void show(AbstractPanel aP) {
        contents.add(aP.constructPanel(), "view");
        cL.show(contents, "view");
    }

    protected void initialise() { // Handles the creation of the menubar
        menuBar = new JMenuBar();

        // Top Left Drop Down Menu
        JMenu gameOptions = new JMenu("Game");
        menuBar.add(gameOptions);

        saveGameButton = new JMenuItem("Save Game");
        saveGameButton.setEnabled(false); // No current game running. Nothing to save

        newGameButton = new JMenuItem("Start New Game");

        loadGameButton = new JMenuItem("Load Game");

        quitToMenu = new JMenuItem("Quit to Main Menu");

        quitGame = new JMenuItem("Quit Game");

        gameOptions.add(saveGameButton);
        gameOptions.addSeparator();
        gameOptions.add(loadGameButton);
        gameOptions.addSeparator();
        gameOptions.add(newGameButton);
        gameOptions.addSeparator();
        gameOptions.add(quitToMenu);
        gameOptions.addSeparator();
        gameOptions.add(quitGame);

        getContentPane().add(BorderLayout.NORTH, menuBar);

    }

    /* Add action listeners for navbar */
    public void enableSaveGame(boolean enabled){
        saveGameButton.setEnabled(enabled);
    }

    public void addSaveGameListener(ActionListener aL){
        saveGameButton.addActionListener(aL);
    }

    public void addNewGameListener(ActionListener aL){
        newGameButton.addActionListener(aL);
    }

    public void addLoadGameListener(ActionListener aL){ loadGameButton.addActionListener(aL); }

    public void addQuitToMenuListener(ActionListener aL){
        quitToMenu.addActionListener(aL);
    }

    public void addQuitGameListener(ActionListener aL){
        quitGame.addActionListener(aL);
    }

    public void addExitWindowListener(WindowListener wL){ this.addWindowListener(wL); }


    /* Getters and Setters */

    public JMenuItem getSaveGame(){
        return saveGameButton;
    }

    public Accuse getAccuse() {
        return accuse;
    }

    public void setAccuse(Accuse accuse) {
        this.accuse = accuse;
    }

    public LoadGame getLoadGame() {
        return loadGame;
    }

    public void setLoadGame(LoadGame loadGame) {
        this.loadGame = loadGame;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public NewGame getNewGame() {
        return newGame;
    }

    public void setNewGame(NewGame newGame) {
        this.newGame = newGame;
    }

    public Refute getRefute() {
        return refute;
    }

    public void setRefute(Refute refute) {
        this.refute = refute;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Suggest getSuggest() {
        return suggest;
    }

    public void setSuggest(Suggest suggest) {
        this.suggest = suggest;
    }

    public void setWelcome(Welcome welcome) {
        this.welcome = welcome;
    }

}