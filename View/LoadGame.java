package View;

import Model.Game;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;

public class LoadGame extends AbstractPanel {

    public LoadGame(GUI view, Game model){
        super(view, model);
    }

    private File loadFile;
    private JLabel fileName;
    private JTextArea string;
    private JButton submit;
    private JButton back;


    @Override
    protected void editTopHeader() {
        JLabel title = new JLabel("<html><b><font size = 20>Load Game</b></html>");
        title.setBorder(new EmptyBorder(10,0,0,0));
        topHeader.add(title);
    }

    @Override
    protected void editBottomHeader() {
        JLabel subtitle = new JLabel("<html><i><font size = 5>Load File or Enter Game String</i></html>");
        submit = new JButton("Submit");

        bottomHeader.add(subtitle);
        bottomHeader.add(submit);
    }

    @Override
    protected void editBody() {
        body.setLayout(new GridLayout(1,2));

        JPanel loadFromFile = new JPanel();
        loadFromFile.setLayout(new BoxLayout(loadFromFile, BoxLayout.PAGE_AXIS));

        JLabel lFFHeader = new JLabel("<html><b><font size = 10>File</b></html>");

        fileName = new JLabel("fileName.txt");
        JButton chooseFile = new JButton("Select File");
        chooseFile.setPreferredSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.4), (int) (GUI.DEFAULT_FRAME_HEIGHT * 0.4)));
        chooseFile.addActionListener(actionEvent -> {
            // Set up file chooser
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("."));
            fileChooser.setDialogTitle("Select save file to load");

            // Make sure user selects a file
            if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                loadFile = fileChooser.getSelectedFile();
                fileName = new JLabel(loadFile.getName());
            }
        });

        loadFromFile.add(lFFHeader);
        loadFromFile.add(chooseFile);
        loadFromFile.add(fileName);

        JPanel loadFromString = new JPanel();
        loadFromString.setLayout(new BoxLayout(loadFromString, BoxLayout.PAGE_AXIS));
        JLabel lFSHeader = new JLabel("<html><b><font size = 10>Text</b></html>");

        string = new JTextArea();
        string.setLineWrap(true);
        string.setMaximumSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.4), (int) (GUI.DEFAULT_FRAME_HEIGHT * 0.4)));
        string.setBorder(new EmptyBorder(10,10,10,10));

        loadFromString.add(lFSHeader);
        loadFromString.add(string);

        body.add(loadFromFile);
        body.add(loadFromString);
    }

    @Override
    protected void editFooter() {
        back = new JButton("Back to menu");
        footer.add(back);
    }

    /**
     * Add action listener to the submit button
     * @param aL  Action listener to add
     */
    public void addSubmitListener(ActionListener aL){
        submit.addActionListener(aL);
    }

    /**
     * Add action listener to the back button
     * @param aL  Action listener to add
     */
    public void addBackListener(ActionListener aL){ back.addActionListener(aL); }

    /**
     * Get the string from the load game text box.
     */
    public String getLoadString(){
        return string.getText();
    }

    /**
     * Get the File object from the file selector object.
     */
    public File getLoadFile(){
        return loadFile;
    }
}
