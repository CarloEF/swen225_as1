package View;
import Model.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Main extends AbstractPanel {
    public JComponent boardPanel;
    public JComponent dicePanel;
    public JComponent characterPanel;
    public JComponent handPanel;

    public JLabel turnLabel;
    public JButton accuse;
    public JButton suggest;
    public JButton endTurn;

    public static double dicePanelWidthPercent = 0.25;
    public static double characterPanelWidthPercent = 0.2;
    public static double handPanelWidthPercent = 0.5;

    public Main(GUI view, Game model){
        super(view, model);
    }

    /**
     * This function checks the current game state and alters the buttons to be disabled
     * or enabled.
     */
    public void setActiveButtons() { ;
        suggest.setEnabled(theModel.getCurrentPlayer().canSuggest());
        accuse.setEnabled(theModel.getCurrentPlayer().canAccuse());
        endTurn.setEnabled(theModel.canEndTurn());
    }

    /**
     * Render the Board.
     */
    public void renderBoardPanel() {
        // Get diamentions
        Dimension d = body.getPreferredSize();

        boardPanel = new JComponent() {
            protected void paintComponent(Graphics g) {
                theModel.getGameBoard().draw(g, 0, 0);
            }
        };

        body.setLayout(new GridBagLayout()); // Vertical align.
        d.setSize(Cell.cellWidth * Board.COLUMNS, Cell.cellHeight * Board.ROWS);
        boardPanel.setBorder(null);
        boardPanel.setPreferredSize(d); // Set side

        // Add it to the drawing:
        body.add(boardPanel);
    }

    /**
     * Render the clickable dice panel for the main window.
     */
    public void renderDicePanel() {
        int footerWidth = footer.getPreferredSize().width;
        int footerHeight = footer.getPreferredSize().height - 10;
        int dicePanelWidth = (int) Math.floor(footerWidth * dicePanelWidthPercent);

        dicePanel = new DrawingBoard() { // Create drawingboard
            protected void paintComponent(Graphics g) { // call the .draw() on all the variables
                // Get the margin locations and element sizes
                int marginLeftStart = (int) Math.floor(dicePanelWidth * 0.225); // Start rendering here
                int diceSize = (int) Math.floor(Math.min(dicePanelWidth * 0.25, footerHeight * 0.20));
                int marginTopStart = (int) Math.floor(footerHeight * 0.1);

                int middleSpacing = (int) Math.floor(dicePanelWidth * 0.05);
                int heightMargin = (int) Math.floor(footerHeight * 0.2);

                // Draw the actual dices
                theModel.diceOne.draw(g,  marginLeftStart, marginTopStart, diceSize);
                theModel.diceTwo.draw(g, marginLeftStart + middleSpacing + diceSize, marginTopStart, diceSize);

                g.setFont(new Font(Font.SERIF, Font.PLAIN, 18));


                String moveString = "Moves: ";
                if(theModel.getGamePhase() != Game.GamePhase.MOVE) {
                    moveString += "---";
                } else {
                    moveString += theModel.getMovesRemaining();
                }


                g.drawString(moveString, marginLeftStart, marginTopStart + diceSize + heightMargin);
            }
        };
        dicePanel.setPreferredSize(new Dimension(dicePanelWidth, footerHeight));
        footer.add(dicePanel);
    }

    /**
     * Render the display of characters on the board.
     */
    public void renderCharacterPanel() {
        int footerWidth = footer.getPreferredSize().width;
        int footerHeight = footer.getPreferredSize().height - 10;
        int characterPanelWidth = (int) Math.floor(footerWidth * characterPanelWidthPercent);

        characterPanel = new DrawingBoard() {
            protected void paintComponent(Graphics g) {
                Player currentPlayer = theModel.getCurrentPlayer();
                currentPlayer.getCharacter().draw(g, 2, 3, 120, footer.getPreferredSize().height - 14, "You:");
            }
        };
        characterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Your Character"));
        characterPanel.setPreferredSize(new Dimension(characterPanelWidth, footerHeight));

        footer.add(characterPanel);
    }


    /**
     * Render the user's hand.
     */
    public void renderHandPanel() {
        int footerWidth = footer.getPreferredSize().width;
        int footerHeight = footer.getPreferredSize().height - 10;
        int handPanelWidth = (int) Math.floor(footerWidth * handPanelWidthPercent);

        // ----- Draw the card hand
        handPanel = new DrawingBoard() {
            protected void paintComponent(Graphics g) {
                Player currentPlayer = theModel.getCurrentPlayer();
                Hand h = currentPlayer.getHand();
                h.draw(g, this.getPreferredSize().width, footer.getPreferredSize().height, null);
            }
        };

        handPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Your Hand"));
        handPanel.setPreferredSize(new Dimension(handPanelWidth, footerHeight));

        footer.add(handPanel);
    }

    @Override
    protected void editTopHeader() {

        turnLabel = new JLabel("Bob's Turn");
        turnLabel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10,20,0,20), BorderFactory.createLineBorder(Color.BLACK)));
        turnLabel.setHorizontalAlignment((int) CENTER_ALIGNMENT);
        topHeader.setLayout(new GridLayout(1,1));
        topHeader.add(turnLabel);
    }

    @Override
    protected void editBottomHeader() {
        bottomHeader.setLayout(new GridLayout(1, 3, 30, 30));
        bottomHeader.setBorder(new EmptyBorder(30,20,20,20)); // Padding!

        suggest = new JButton("Suggest");
        suggest.setEnabled(false);
        accuse = new JButton("Accuse");
        accuse.setEnabled(false);
        endTurn = new JButton("End Turn");

        bottomHeader.add(suggest);
        bottomHeader.add(accuse);
        bottomHeader.add(endTurn);

        this.setActiveButtons();
    }

    @Override
    protected void editBody() {
        renderBoardPanel();
    }

    @Override
    protected void editFooter() {
        FlowLayout layout = new FlowLayout();
        layout.setHgap(0);
        layout.setVgap(0);
        footer.setLayout(layout);

        renderDicePanel();
        renderCharacterPanel();
        renderHandPanel();
    }


}
