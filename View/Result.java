package View;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import Model.Game;

public class Result extends AbstractPanel {

	public Result(GUI view, Game model){
        super(view, model);
    }

    private JButton mainMenu;

    @Override
    protected void editTopHeader() {
    	JLabel title;
    	if(!theModel.checkAllDead()) {
    		//Show winner tag
    		title = new JLabel("<html><b><font size = 20>WE HAVE A  WINNER!</b></html>");
    	}
    	else {
    		//Show Losers' tag
    		title = new JLabel("<html><b><font size = 20>EVERYONE HAS BEEN MURDERED!</b></html>");
    	}
        title.setBorder(new EmptyBorder(10,0,0,0));
        topHeader.add(title);
    }

    @Override
    protected void editBottomHeader() {
    	JLabel subtitle;
    	if(!theModel.checkAllDead()) {
    		//Show the winner name
    		subtitle = new JLabel("<html><i><font size = 5>"+theModel.getCurrentPlayer().getName()+"</i></html>");
    	}
    	else {
    		//No one has won the game
    		subtitle = new JLabel("<html><i><font size = 2>The Murder Circumstances were:\n"+theModel.getSolution().getDescription()+"</i></html>");
    	}
        bottomHeader.add(subtitle);
    }

    @Override
    protected void editBody() {
        mainMenu = new JButton("Back to Main Menu");
        mainMenu.setPreferredSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.4), (int) (GUI.DEFAULT_FRAME_HEIGHT *0.5)));

        body.add(mainMenu);
    }

    @Override
    protected void editFooter() {
    }

    public void addMainMenuListener(ActionListener aL){
        mainMenu.addActionListener(aL);
    }

}
