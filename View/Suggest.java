package View;

import javax.swing.*;

import Model.*;
import Model.Card.Type;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import static View.GUI.DEFAULT_FRAME_WIDTH;

public class Suggest extends AbstractPanel {

    JButton submit;
    private Card room;
    private Card character;
    private Card weapon;
    private Circumstance suggestion;

    public Suggest(GUI view, Game model){
        super(view, model);
    }

    @Override
    protected void editTopHeader() {

    }

    @Override
    protected void editBottomHeader() {
        submit = new JButton("Submit");
        bottomHeader.add(submit);
        submit.setEnabled(false);
        submit.setVisible(true);
    }


    private void createSuggestPanel(JPanel suggestion){
        suggestion.setLayout(new BoxLayout(suggestion, BoxLayout.Y_AXIS));
        suggestion.setBorder(genericBorder);
    }

    @Override
    protected void editBody() {
    	body.add(BorderLayout.NORTH, new JLabel("What is your Suggestion?..."));
    	//Character Selection
    	JPanel characterSuggest = new JPanel();
        characterSuggest.add(new JLabel("Character"));
        createSuggestPanel(characterSuggest);

        //panel for weapon suggestion
        JPanel weaponSuggest = new JPanel();
        weaponSuggest.add(new JLabel("Weapon"));
        createSuggestPanel(weaponSuggest);

        //panel for room suggestion
        JPanel roomSuggest = new JPanel();
        roomSuggest.add(new JLabel("Room"));
        createSuggestPanel(roomSuggest);

        //Setup Radio Buttons for each room
    	ArrayList<Card> roomCards = Card.getPlayableCards(Type.ROOM);
        ArrayList<Card> weaponCards = Card.getPlayableCards(Type.WEAPON);
        ArrayList<Card> characterCards = Card.getPlayableCards(Type.CHARACTER);

        // This action listener will be assigned to each radio button.
        ActionListener setSuggestion = actionEvent -> {
            Card c = Card.getCardFromCharacter(actionEvent.getActionCommand());
            if (c.getType() == Type.ROOM){ room = c; }
            else if (c.getType() == Type.WEAPON){ weapon = c; }
            else if (c.getType() == Type.CHARACTER){ character = c; }

            if (room != null && weapon != null && character != null){
                submit.setEnabled(true);
                suggestion = new Circumstance(character, room, weapon);
            }

            else { submit.setEnabled(false); }
        };
        /*     Creating the room radio buttons   */
    	JRadioButton[] roomButtons = new JRadioButton[roomCards.size()];
    	ButtonGroup roomGroup = new ButtonGroup();

    	for(int i = 0; i < roomCards.size(); i++) {
            roomButtons[i] = new JRadioButton(roomCards.get(i).getDescription());
            roomButtons[i].addActionListener(setSuggestion);
            roomButtons[i].setActionCommand(roomCards.get(i).toString());
            if(Card.getCardFromCharacter(roomCards.get(i).toString()) == theModel.getCurrentPlayer().getCurrentCell().getRoom()){
                roomButtons[i].setSelected(true);
                room = Card.getCardFromCharacter(roomCards.get(i).toString());
            }
            roomButtons[i].setEnabled(false);
    		roomGroup.add(roomButtons[i]);
    	}

        /*     Creating the weapon radio buttons   */
        JRadioButton[] weaponButtons = new JRadioButton[weaponCards.size()];
        ButtonGroup weaponGroup = new ButtonGroup();

        // Add weapon radio buttons
        for(int i = 0; i< weaponCards.size(); i++) {
            weaponButtons[i] = new JRadioButton(weaponCards.get(i).getDescription());
            weaponButtons[i].addActionListener(setSuggestion);
            weaponButtons[i].setActionCommand(weaponCards.get(i).toString());
            weaponGroup.add(weaponButtons[i]);
        }

        // Add character radio buttons
        JRadioButton[] characterButtons = new JRadioButton[characterCards.size()];
        ButtonGroup characterGroup = new ButtonGroup();

        for(int i = 0; i < characterCards.size(); i++) {
            characterButtons[i] = new JRadioButton(characterCards.get(i).getDescription());
            characterButtons[i].addActionListener(setSuggestion);
            characterButtons[i].setActionCommand(characterCards.get(i).toString());
            characterGroup.add(characterButtons[i]);
        }
    	for(JRadioButton b :roomButtons)
            roomSuggest.add(b);
    	roomSuggest.setVisible(true);
    	body.add(roomSuggest);

        for(JRadioButton b :weaponButtons)
            weaponSuggest.add(b);
        weaponSuggest.setVisible(true);
        body.add(weaponSuggest);

        for(JRadioButton b :characterButtons)
            characterSuggest.add(b);
        characterSuggest.setVisible(true);
        body.add(characterSuggest);
    }

    /**
     * Adds the characters hand to the footer
     */
    @Override
    protected void editFooter() {
        JComponent handPanel = new JComponent() {
            protected void paintComponent(Graphics g) {
                Player currentPlayer = theModel.getCurrentPlayer();
                Hand h = currentPlayer.getHand();
                h.draw(g, (int) (this.getPreferredSize().width * 0.9), (int) (this.getPreferredSize().height * 0.8), null);
            }
        };

        handPanel.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, (int) footer.getPreferredSize().getHeight()));
        handPanel.setVisible(true);

        footer.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Your Hand"));
        footer.add(handPanel);
    }

    public Circumstance getSuggestion() {
        return suggestion;
    }

    public void addSubmitListener(ActionListener aL){
        submit.addActionListener(aL);
    }

}
