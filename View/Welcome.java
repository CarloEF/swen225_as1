package View;

import Model.Game;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class Welcome extends AbstractPanel {

    public Welcome(GUI view, Game model){
        super(view, model);
    }

    private JButton newGame;
    private JButton loadGame;

    @Override
    protected void editTopHeader() {
        JLabel title = new JLabel("<html><b><font size = 20>CLUEDO</b></html>");
        title.setBorder(new EmptyBorder(10,0,0,0));
        topHeader.add(title);
    }

    @Override
    protected void editBottomHeader() {
        JLabel subtitle = new JLabel("<html><i><font size = 5>The Great Detective Game</i></html>");
        bottomHeader.add(subtitle);
    }

    @Override
    protected void editBody() {
        newGame = new JButton("Start new game");
        newGame.setPreferredSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.4), (int) (GUI.DEFAULT_FRAME_HEIGHT *0.5)));

        loadGame = new JButton("Load game from file");
        loadGame.setPreferredSize(new Dimension((int) (GUI.DEFAULT_FRAME_WIDTH * 0.4), (int) (GUI.DEFAULT_FRAME_HEIGHT *0.5)));

        body.add(newGame);
        body.add(loadGame);
    }

    @Override
    protected void editFooter() {
    }

    public void addNewGameListener(ActionListener aL){
        newGame.addActionListener(aL);
    }

    public void addLoadGameListener(ActionListener aL){
        loadGame.addActionListener(aL);
    }
}
